let path = require('path');

const fs = require('fs');

module.exports = class {
  static async init(cms) {
    try {
      let page = cms.content_manager.serve_extra_page("Documentation", path.resolve(__dirname, 'dist'));

      const doc_page = "/Documentation/"
      let doc_page_obj = cms.page_list.select_obj("Documentation");

      cms.app.post("/docreator-control", cms.admin.auth.orize_gen(["content_management"]), async function(req, res) {
        try {
          var data = JSON.parse(req.body.data);
          let page_path = path.resolve(cms.app_path, "pages"+doc_page);

          switch (data.command) {
            case 'add_page':
              fs.writeFileSync(path.resolve(page_path, "asciidoc/pages/"+data.name+".mdoc"), "## Heading");
              let dpages = JSON.parse(fs.readFileSync(path.resolve(page_path, "pages.json"), "utf8"));
              dpages.list.push(data.name+".mdoc");
              fs.writeFileSync(path.resolve(page_path, "pages.json"), JSON.stringify(dpages));
              doc_page_obj.serve_directory();
              res.json(dpages);

              break;
            case 'delete_page':
              if (data.name != "index") {
                fs.unlinkSync(path.resolve(page_path, "asciidoc/pages/"+data.name+".mdoc"));
                let upages = JSON.parse(fs.readFileSync(path.resolve(page_path, "pages.json"), "utf8"));
                upages.list.splice(upages.list.indexOf(data.name+".mdoc"), 1);
                fs.writeFileSync(path.resolve(page_path, "pages.json"), JSON.stringify(upages));
                res.json(upages);
              } else {
                res.send("ERROR: can't delete Main doc page!");
              }
              break;
            case 'save_page':
              if (data.name != "index") {
                fs.writeFileSync(path.resolve(page_path, "asciidoc/pages/"+data.name+".mdoc"), data.content);
              } else {
                fs.writeFileSync(path.resolve(page_path, "asciidoc/"+data.name+".mdoc"), data.content);
              }
              let spages = JSON.parse(fs.readFileSync(path.resolve(page_path, "pages.json"), "utf8"));
              res.json(spages)

              break;
            default:

          }
        } catch (e) {
          console.error(e.stack);
        }
      });
      return new module.exports();
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  constructor() {

  }
}
