const XHR = require("core/utils/xhr_async.js");
const asciidoctor = require('asciidoctor')(); 
const content = 'http://asciidoctor.org[*Asciidoctor*] ' +
  'running on https://opalrb.com[_Opal_] ' +
  'brings AsciiDoc to Node.js!'
const html = asciidoctor.convert(content) 

const mdoc_page_path = "/Documentation/";

const CodeMirror = require("core/codemirror.ui/index.js");


let mdoc_nav = document.getElementById("mdoc-nav");
let mdoc_nav_new_name = document.getElementById("mdoc-nav-new-name");
let mdoc_nav_add_new = document.getElementById("mdoc-nav-add-new");

let mdoc_content = document.getElementById("mdoc-content");
let mdoc_main_link = document.getElementById("mdoc-main-link");

let mdoc_save = document.getElementById("mdoc-save");
let mdoc_delete = document.getElementById("mdoc-delete");

let editor_div = document.getElementById("mdoc-editor");
let editor = new CodeMirror('', '', false);
editor_div.appendChild(editor.element);

let cur_mdoc = "index";

class Page {
  constructor(file_name, mdoc) {
    this.name = file_name.replace(/\.mdoc(?!.*\.mdoc)/, "");
    this.element = document.createElement("a");
    this.element.innerHTML = this.name;
    this.element.href = "#"+file_name;
    let this_class = this;
    this.element.addEventListener("click", function(e) {
      cur_mdoc = this_class.name;
      editor.cm.setValue(mdoc);
      mdoc_content.innerHTML = asciidoctor.convert(mdoc);
      highlight_code();
    });
    mdoc_nav.appendChild(this.element);
  }

  destroy() {
    mdoc_nav.removeChild(this.element);
  }
} 


function highlight_code() {
  document.querySelectorAll('pre code').forEach((block) => {
    hljs.highlightBlock(block);
  });
}

const docreator_control = "/docreator-control";

async function display_main() {
  let main_mdoc = await XHR.getText(mdoc_page_path+"asciidoc/index.mdoc");
  cur_mdoc = "index";
  editor.cm.setValue(main_mdoc);
  mdoc_content.innerHTML = asciidoctor.convert(main_mdoc);
}

async function display(name) {
  let mdoc = await XHR.getText(mdoc_page_path+"asciidoc/pages/"+name+".mdoc"); 
  cur_mdoc = name;
  editor.cm.setValue(mdoc);
  mdoc_content.innerHTML = asciidoctor.convert(mdoc);
  highlight_code();
}

(async function() {
  try {
    let pagel = (await XHR.get(mdoc_page_path+"pages.json")).list;
    display_main();
    highlight_code();

    mdoc_main_link.addEventListener("click", async function(e) {
      await display_main();
    });

    let pages = [];
    async function load_pages(page_list) {
      try {
        for (let p = 0; p < pages.length; p++) {
          pages[p].destroy();
        }

        pages = [];

        console.log("load page", page_list);

        for (let p = 0; p < page_list.length; p++) {
          let mdoc = await XHR.getText(mdoc_page_path+"asciidoc/pages/"+page_list[p]);
          pages.push(new Page(page_list[p], mdoc));
        }

      } catch (e) {
        console.error(e.stack);
      }
    }
    await load_pages(pagel);

    mdoc_nav_add_new.addEventListener("click", async function(e) {
      try {
        await load_pages((await XHR.post(docreator_control, {
          command: "add_page",
          name: mdoc_nav_new_name.value
        }, "access_token")).list);
        display(mdoc_nav_new_name.value);
      } catch(e) {
        console.error(e.stack);
      }
    });

    mdoc_save.addEventListener("click", async function(e) {
      try {
        await load_pages((await XHR.post(docreator_control, {
          command: "save_page",
          name: cur_mdoc,
          content: editor.cm.getValue()
        }, "access_token")).list);
      } catch(e) {
        console.error(e.stack);
      }
    });

    mdoc_delete.addEventListener("click", async function(e) {
      try {
        if (confirm('Are you sure you want to delete this documantation page?')) {
          await load_pages((await XHR.post(docreator_control, {
            command: "delete_page",
            name: cur_mdoc
          }, "access_token")).list);
          display_main();
        } else {
            // Do nothing!
        }
      } catch(e) {
        console.error(e.stack);
      }
    });


  } catch (e) {
    console.error(e.stack);
  }
})();

let update_required = false;

editor.cm.on("change", function(e) {
  update_required = true;
});

setInterval(function() {
  if (update_required) {
    update_required = false;
    mdoc_content.innerHTML = asciidoctor.convert(editor.cm.getValue());
    highlight_code();
  }
}, 2000);

